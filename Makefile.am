# Process this file with automake to generate Makefile.in
#
# buildutils-q2bsp: Compile levels for Quake II
# Copyright (C) 2005-2006 Peter Brett
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

q2bsp_srcdir = src/q2bsp
q2bspinfo_srcdir = src/q2bspinfo
q2vis_srcdir = src/q2vis
q2rad_srcdir = src/q2rad
common_srcdir = src/common

common_headers = $(common_srcdir)/bspfile.h \
$(common_srcdir)/cmdlib.h \
$(common_srcdir)/l3dslib.h \
$(common_srcdir)/lbmlib.h \
$(common_srcdir)/mathlib.h \
$(common_srcdir)/memory.h \
$(common_srcdir)/parsecfg.h \
$(common_srcdir)/polylib.h \
$(common_srcdir)/qfiles.h \
$(common_srcdir)/scriplib.h \
$(common_srcdir)/threads.h \
$(common_srcdir)/trilib.h \
$(common_srcdir)/hash.h

common_srcs = $(common_srcdir)/bspfile.c \
$(common_srcdir)/cmdlib.c \
$(common_srcdir)/l3dslib.c \
$(common_srcdir)/lbmlib.c \
$(common_srcdir)/mathlib.c \
$(common_srcdir)/memory.c \
$(common_srcdir)/parsecfg.c \
$(common_srcdir)/polylib.c \
$(common_srcdir)/scriplib.c \
$(common_srcdir)/threads.c \
$(common_srcdir)/trilib.c \
$(common_srcdir)/gamedata.c \
$(common_srcdir)/hash.c

INCLUDES = -I$(common_srcdir)

bin_PROGRAMS = q2bsp q2bspinfo q2vis q2rad
q2bsp_SOURCES = $(common_headers) $(common_srcs) \
$(q2bsp_srcdir)/brushbsp.c \
$(q2bsp_srcdir)/csg.c \
$(q2bsp_srcdir)/faces.c \
$(q2bsp_srcdir)/glfile.c \
$(q2bsp_srcdir)/leakfile.c \
$(q2bsp_srcdir)/map.c \
$(q2bsp_srcdir)/nodraw.c \
$(q2bsp_srcdir)/portals.c \
$(q2bsp_srcdir)/prtfile.c \
$(q2bsp_srcdir)/q2bsp.c \
$(q2bsp_srcdir)/textures.c \
$(q2bsp_srcdir)/tree.c \
$(q2bsp_srcdir)/writebsp.c \
$(q2bsp_srcdir)/q2bsp.h

q2bspinfo_SOURCES = $(common_headers) $(common_srcs) \
$(q2bspinfo_srcdir)/q2bspinfo.c

q2vis_SOURCES = $(common_headers) \
$(common_srcdir)/bspfile.c \
$(common_srcdir)/cmdlib.c \
$(common_srcdir)/l3dslib.c \
$(common_srcdir)/lbmlib.c \
$(common_srcdir)/mathlib.c \
$(common_srcdir)/memory.c \
$(common_srcdir)/parsecfg.c \
$(common_srcdir)/scriplib.c \
$(common_srcdir)/threads.c \
$(common_srcdir)/trilib.c \
$(q2vis_srcdir)/flow.c \
$(q2vis_srcdir)/q2vis.c \
$(q2vis_srcdir)/q2vis.h

q2rad_SOURCES = $(common_headers) $(common_srcs) \
$(q2rad_srcdir)/lightmap.c \
$(q2rad_srcdir)/patches.c \
$(q2rad_srcdir)/q2rad.c \
$(q2rad_srcdir)/trace.c \
$(q2rad_srcdir)/q2rad.h
