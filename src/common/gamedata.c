/* gamedata.c -- Find & load game data files
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "cmdlib.h"
#include "hash.h"
#include "qfiles.h"

/* Subdirectory of game directory containing standard game data files */
#define BASEDATADIR "baseq2"

char q2path[FN_SIZE];
char modpath[FN_SIZE];


#define MAXDATAFILES (1<<13)	/* Max number of data files indexed */
#define PAKFN_SIZE 0x38		/* Length of filenames stored in a PAK */

/* Where can files be stored? */
enum location
{ ISPAK = 1, ISMOD = 2 };


typedef struct
{
  char filename[PAKFN_SIZE];	/* Name of file, with extension. 56 chars
				 * padded with \0 */
  int where;
  FILE *pak;			/* File pointer of PAK file that file's in (if applicable) */
  int32_t offset, size;		/* Location & size in PAK file (if applicable) */
} datafile_t;

datafile_t filetable[MAXDATAFILES];	/* Hashtable of data files */
int filetable_collisions, filetable_count, filetable_seed;


void Index_CleanFN (char *filename);
int Index_HashFN (const char *filename);
void Index_Add (datafile_t fileinfo);
int Index_Search (char *filename);
void Index_ScanPAK (FILE * pakfile, const int where);
int Index_ScanPAKDir (const char *path, int where);
void Index_ScanDir (const char *dir, const char *subpath, int where);

/*
===================
InitGameData

Search for PAK files and index their contents. Files are chosen from
different locations in order of priority (highest to lowest):

 (1) individual files in moddir
 (2) PAK files in moddir
 (3) individual files in base data dir
 (4) PAK files in base data dir
===================
*/
void
InitGameData (char *gamedir, char *moddir)
{
  int numpaks = 0;

  qprintf("\n--- InitGameData ---\n");

  /* Initialize hashtable */
  filetable_collisions = filetable_count = 0;
  filetable_seed = rand ();
  memset (filetable, 0, MAXDATAFILES * sizeof (datafile_t));

  /* Base game data */
  strncpy (q2path, gamedir, FN_SIZE);
  BuildPath (q2path, BASEDATADIR);
  numpaks += Index_ScanPAKDir (q2path, 0);
  Index_ScanDir (q2path, "", 0);

  /* Mod-specific data */
  if (moddir[0])
    {
      strncpy(modpath, moddir, FN_SIZE);
      numpaks += Index_ScanPAKDir (modpath, ISMOD);
      Index_ScanDir (modpath, "", ISMOD);
    }

  qprintf ("Found %i files (%i PAKs, %i collisions)\n", filetable_count,
	   numpaks, filetable_collisions);
}

/*
===================
LoadGameData

Load a game data file. Returns -1 if not possible to load file
(e.g. file not found). If print_error evaluates to true, also prints
an error message on failure. Returns filesize on success.
===================
*/
int
LoadGameData (char *filename, void **bufferptr, int print_error)
{
  int infoindex, length;
  datafile_t *fileinfo;
  void *buffer;
  FILE * f;

  *bufferptr = NULL;

  /* Search hashtable for requested filename */
  infoindex = Index_Search(filename);
  if (infoindex == -1)
    {
      if (print_error)
	  printf("Warning: Couldn't find %s\n", filename);
      return -1;
    }

  fileinfo = &filetable[infoindex];

  if (fileinfo->where & ISPAK)
    {
      /* It's part of a PAK file */
      f = fileinfo->pak;
      fseek(f, fileinfo->offset, SEEK_SET);
      if (!fseek (f, fileinfo->offset, SEEK_SET))
	{
	  length = fileinfo->size;
	}
      else
	{
	  Error ("Failed to seek in PAK file\n");
	}      
    }
  else
    {
      /* It's a standalone file */
      f = fopen (filename, "rb");
      
      if (!f)
	{
	  if (print_error)
	    printf ("Warning: Couldn't open %s\n", filename);
	  return -1;
	}
      
      length = Q_filelength (f);
    }


  buffer = malloc(length + 1);
  if (buffer == NULL)
    {
      return -1;
    }
  ((char *) buffer)[length] = 0;
  SafeRead (f, buffer, length);
  *bufferptr = buffer;

  if (!(fileinfo->where & ISPAK))
    {
      /* We don't close PAK files, we leave them open for any future
	 reads. */
      fclose (f);
    }
      
  return length;
}

/*
===================
Index_CleanFN

Clean up a filename (replace file separators, etc) ready for indexing.
===================
*/
void
Index_CleanFN (char *filename)
{
  int n;
  n = strlen (filename);

  if ((n == 0) || (n > PAKFN_SIZE) || IsAbsolutePath (filename))
    Error ("Bad filename for index: %s\n", filename);

  for (n--; n >= 0; n--)
    {
      if (IsPathSeparator (filename[n]))
	{
	  filename[n] = PATHSEPARATOR;
	}
    }
}

/*
===================
Index_HashFN

Create a hash code from a filename for indexing.
===================
*/
int
Index_HashFN (const char *filename)
{
  int result, n;

  n = strlen (filename);
  result = (int) hash ((uint8_t *) filename, n, filetable_seed);
  result = abs (result) % MAXDATAFILES;

  // printf("%X: %s (%s)\n", result, filename, filebase);
  return result;
}

/*
===================
Index_Add

Add a file to the hashmap. If the filename is already present,
overwrite it.
===================
*/
void
Index_Add (datafile_t fileinfo)
{
  int hash, n;
  Index_CleanFN (fileinfo.filename);
  hash = Index_HashFN (fileinfo.filename);

  /* Don't even try if the hashtable is full */
  if (filetable_count == MAXDATAFILES)
    {
      Error ("Too many game data files. Increase MAXDATAFILES.\n");
      return;
    }

  n = hash;
  while (1)
    {
      /* Do we create a new entry? */
      if (!filetable[n].filename[0])
	{
	  memcpy (&filetable[n], &fileinfo, sizeof (datafile_t));
	  filetable_count++;
	  return;
	}

      /* Do we overwrite an old entry? */
      if (!strncasecmp (filetable[n].filename, fileinfo.filename, PAKFN_SIZE))
	{
	  memcpy (&filetable[n], &fileinfo, sizeof (datafile_t));
	  return;
	}

      n = ++n % MAXDATAFILES;
      filetable_collisions++;	/* Keep track of how many collisions we get */

      /* In theory this should never become an infinite loop, because
       * we already checked if the hashtable was full. But we check
       * anyway. */
      if (n == hash)
	{
	  Error ("Too many game data files. Increase MAXDATAFILES.\n");
	  return;
	}
    }
}

/*
===================
Index_Search

Find a filename in the hashmap. Returns the index of a record in the
hashmap. Returns -1 if not present.
===================
*/
int
Index_Search (char *filename)
{
  int hash, n;
  Index_CleanFN (filename);
  hash = Index_HashFN (filename);

  n = hash;
  while (1)
    {
      if (!filetable[n].filename[0])
	{
	  return -1;
	}

      if (!strncasecmp (filetable[n].filename, filename, PAKFN_SIZE))
	{
	  return n;
	}

      n = ++n % MAXDATAFILES;
      if (n == hash)
	{
	  /* We looped all the way through without finding it */
	  return -1;
	}
    }
}

/*
===================
Index_ScanPAK

Scans a PAK file's contents and adds file info to the index.
===================
*/
void
Index_ScanPAK (FILE * pakfile, const int where)
{
  dpackheader_t pakheader;
  dpackfile_t *pakentries;
  datafile_t fileinfo;
  int numfiles, i;

  fileinfo.where = where | ISPAK;
  fileinfo.pak = pakfile;

  fseek (pakfile, 0, SEEK_SET);

  SafeRead (pakfile, &pakheader, sizeof (dpackheader_t));

  /* Read PAK files correctly on big-endian archs */
  pakheader.ident = LittleLong(pakheader.ident);
  pakheader.dirofs = LittleLong(pakheader.dirofs);
  pakheader.dirlen = LittleLong(pakheader.dirlen);

  /* Idiocy check */
  if (pakheader.ident != IDPAKHEADER)
    {
      printf ("Warning: Unsupported PAK file format\n");
      return;
    }

  pakentries = malloc (pakheader.dirlen);
  memset (pakentries, 0, pakheader.dirlen);

  numfiles = pakheader.dirlen / sizeof (dpackfile_t);

  if (pakentries != NULL)
    {
      if (!fseek (pakfile, pakheader.dirofs, SEEK_SET))
	{
	  SafeRead (pakfile, pakentries, pakheader.dirlen);
	  for (i = 0; i < numfiles; i++)
	    {
	      strncpy (fileinfo.filename, pakentries[i].name, PAKFN_SIZE);
	      fileinfo.offset = LittleLong(pakentries[i].filepos);
	      fileinfo.size = LittleLong(pakentries[i].filelen);
	      Index_Add (fileinfo);
	    }
	}
    }

  /* Make sure we free up any allocated memory */
  if (pakentries != NULL)
    {
      free (pakentries);
    }
}

/*
===================
Index_ScanPAKDir

Scans a directory for PAK files. Returns the number of PAK files
indexed.
===================
*/
int
Index_ScanPAKDir (const char *path, int where)
{
  FILE *f;
  int n, base_len;
  char pakfile[FN_SIZE];
  char pakpath[FN_SIZE];
  int pakwhere = where | ISPAK;

  strncpy (pakpath, path, FN_SIZE);
  base_len = strlen (pakpath);

  for (n = 0;; n++)
    {
      /* Truncate path back to the base path passed in, then add the
       * name of the next pak file */
      sprintf (pakfile, "pak%d.pak", n);
      pakpath[base_len] = 0;
      BuildPath (pakpath, pakfile);

      f = fopen (pakpath, "rb");

      if (!f)
	{
	  if (errno == ENOENT)
	    {
	      return n;
	    }
	  /* Print a warning but don't fail, because there might be
	   * more, readable pak files */
	  perror ("Couldn't open PAK file");
	  continue;
	}

      qprintf ("Scanning %s...\n", pakpath);
      Index_ScanPAK (f, pakwhere);
    }
}

/*
===================
Index_ScanDir

Scan a directory for regular files that might override PAK file
contents.
===================
*/
void
Index_ScanDir (const char *dir, const char *subpath, int where)
{
  char path[FN_SIZE];
  char relpath[FN_SIZE];
  struct stat filestat;
  int subpath_len;
  DIR *directory;
  struct dirent *ep;
  datafile_t fileinfo;

  strncpy (relpath, subpath, FN_SIZE);
  strncpy (path, dir, FN_SIZE);
  BuildPath (path, relpath);
  stat (path, &filestat);

  if (S_ISREG (filestat.st_mode))
    {
      /* This is a regular file, add to the index.
       * FIXME: Skip PAK files */

      fileinfo.where = where & (~ISPAK);
      Index_CleanFN (relpath);	/* Need to do this to make avoid segfault */

      strncpy (fileinfo.filename, relpath, PAKFN_SIZE);
      Index_Add (fileinfo);
      return;
    }

  if (S_ISDIR (filestat.st_mode))
    {
      /* This is a directory, recurse */
      directory = opendir (path);

      if (directory == NULL)
	{
	  printf ("Warning: Couldn't open directory %s\n", path);
	  return;
	}

      subpath_len = strlen (relpath);

      while (ep = readdir (directory))
	{
	  if (ep->d_name[0] == '.')
	    {
	      /* Skip hidden files, '.' and '..' */
	      continue;
	    }
	  BuildPath (relpath, (ep->d_name));
	  Index_ScanDir (dir, relpath, where);
	  relpath[subpath_len] = 0;
	}
      closedir (directory);
    }

}
