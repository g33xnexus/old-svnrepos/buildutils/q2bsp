/* memory.c 
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef _DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#define MEG 1048576

static long cnt = 0;
static long chunk = 0;
static long total = 0;
static long cur = 0;
static long peak = 0;
static long next = MEG;

void *
GetMemory (long size)
{
  cnt++;

  if (size > chunk)
    chunk = size;

  cur += size;
  total += size;

  if (peak < cur)
    peak = cur;

  if (next <= peak)
    {
      printf ("\n%d Megs currently\n", peak / MEG);

      next += MEG;
    }

  return malloc (size);
}

void
FreeMemory (void *p)
{
  cur -= _msize (p);

  free (p);
}

void
FinalReport ()
{
  printf ("%15ld Chunks.\n", cnt);
  printf ("%15ld Biggest chunk.\n", chunk);
  printf ("%15ld Average chunk.\n", total / cnt);
  printf ("%15ld Total memory.\n", total);
  printf ("%15ld Peak memory.\n", peak);

  getchar ();
}

#endif
