/* lbmlib.h
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef COMMON_LBMLIB_H
#define COMMON_LBMLIB_H

void LoadLBMFile (char *filename, byte ** picture, byte ** palette);
void LoadLBMBuffer (byte *LBMbuffer, byte ** picture, byte ** palette);
void WriteLBMfile (char *filename, byte * data, int width, int height,
		   byte * palette);
void LoadPCXFile (char *filename, byte ** picture, byte ** palette, int *width,
		  int *height);
void LoadPCXBuffer (byte *PCXbuffer, int buffersize, byte ** pic, 
		    byte ** palette, int *width, int *height);
void WritePCXfile (char *filename, byte * data, int width, int height,
		   byte * palette);

// loads / saves either lbm or pcx, depending on extension
void Load256Image (char *name, byte ** pixels, byte ** palette,
		   int *width, int *height);
void Save256Image (char *name, byte * pixels, byte * palette,
		   int width, int height);


void LoadTGA (char *filename, byte ** pixels, int *width, int *height);

#endif /* !COMMON_LBMLIB_H */
