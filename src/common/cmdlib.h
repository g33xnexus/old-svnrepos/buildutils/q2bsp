/* cmdlib.h -- General function library
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef COMMON_CMDLIB_H
#define COMMON_CMDLIB_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef _WIN32
#pragma warning(disable : 4244)	// MIPS
#pragma warning(disable : 4136)	// X86
#pragma warning(disable : 4051)	// ALPHA

#pragma warning(disable : 4018)	// signed/unsigned mismatch
#pragma warning(disable : 4305)	// truncate from double to float
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <stdarg.h>
#include <stdint.h>
#include <getopt.h>

#ifndef __BYTEBOOL__
#define __BYTEBOOL__
typedef enum
{ false, true } qboolean;
typedef unsigned char byte;
#endif

#ifndef WIN32
#define PATHSEPARATOR   '/'
#else /* WIN32 */
#define PATHSEPARATOR   '\\'
#endif /* WIN32 */

/* Max length of strings containing filenames */
#define FN_SIZE 1024

// the dec offsetof macro doesnt work very well...
#define myoffsetof(type,identifier) ((size_t)&((type *)0)->identifier)


// set these before calling CheckParm
extern int myargc;
extern char **myargv;

char *strupr (char *in);
char *strlower (char *in);
int Q_strcasecmp (char *s1, char *s2);
void Q_getwd (char *out);

int Q_filelength (FILE * f);
int FileTime (char *path);

void Q_mkdir (char *path);

char *ExpandArg (char *path);	// from cmd line

double I_FloatTime (void);

void Error (char *error, ...);
int CheckParm (char *check);
int IsPathSeparator (const char c);
int IsAbsolutePath (const char *path);
int BuildPath (char *path1, const char *path2);

FILE *SafeOpenWrite (char *filename);
FILE *SafeOpenRead (char *filename);
void SafeRead (FILE * f, void *buffer, int count);
void SafeWrite (FILE * f, void *buffer, int count);

int LoadFile (char *filename, void **bufferptr);
int TryLoadFile (char *filename, void **bufferptr, int print_error);
void SaveFile (char *filename, void *buffer, int count);
qboolean FileExists (char *filename);

void DefaultExtension (char *path, char *extension);
void DefaultPath (char *path, char *basepath);
void StripFilename (char *path);
void StripExtension (char *path);

void ExtractFilePath (char *path, char *dest);
void ExtractFileBase (char *path, char *dest);
void ExtractFileExtension (char *path, char *dest);

int ParseNum (char *str);

short BigShort (short l);
short LittleShort (short l);
int BigLong (int l);
int LittleLong (int l);
float BigFloat (float l);
float LittleFloat (float l);
char *COM_Parse (char *data);

extern char com_token[1024];
extern qboolean com_eof;

char *copystring (char *s);


void CRC_Init (unsigned short *crcvalue);
void CRC_ProcessByte (unsigned short *crcvalue, byte data);
unsigned short CRC_Value (unsigned short crcvalue);

void CreatePath (char *path);
void QCopyFile (char *from, char *to);

extern qboolean archive;
extern char archivedir[FN_SIZE];


extern qboolean verbose;
void qprintf (char *format, ...);

void ExpandWildcards (int *argc, char ***argv);


// for compression routines
typedef struct
{
  byte *data;
  int count;
} cblock_t;


extern int context;

void ShowVersion (char *progname);

// ----------------------

void InitGameData (char *gamedir, char *moddir);
int LoadGameData (char *filename, void **bufferptr, int print_error);
#endif /* !COMMON_CMDLIB_H */
