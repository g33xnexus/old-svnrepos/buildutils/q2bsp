/* hash.h -- Generic fast hash function
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 2005-2006 Peter Brett
 * Based on http://burtleburtle.net/bob/c/lookup2.c (public domain)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef COMMON_HASH_H
#define COMMON_HASH_H

#include "cmdlib.h"

uint32_t hash (uint8_t * k, uint32_t length, uint32_t initval);

#endif /* !COMMON_HASH_H */
