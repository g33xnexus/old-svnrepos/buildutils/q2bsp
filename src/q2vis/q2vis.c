/* q2vis.c
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "q2vis.h"
#include "threads.h"
#include "stdlib.h"
#include "parsecfg.h"

#define Q2VIS_STRING "Q2VIS"

int numportals;
int portalclusters;

portal_t *portals;
leaf_t *leafs;

int c_portaltest, c_portalpass, c_portalcheck;

byte *uncompressedvis;

byte *vismap, *vismap_p, *vismap_end;	// past visfile
int originalvismapsize;

int leafbytes;			// (portalclusters+63)>>3
int leaflongs;

int portalbytes, portallongs;

qboolean fastvis;
qboolean nosort;

int testlevel = 2;
float maxdist = 0.0f;

int totalvis;

portal_t *sorted_portals[MAX_MAP_PORTALS * 2];

/* *INDENT-OFF* */
static const struct option long_options[] =
  {
    {"cullerror",  no_argument,       0, 'c'},
    {"maxdist",    required_argument, 0, 'd'},
    {"fast",       no_argument,       0, 'f'},
    {"help",       no_argument,       0, 'h'},
    {"maxfovdist", required_argument, 0, 'm'},
    {"nosort",     no_argument,       0, 's'},
    {"threads",    required_argument, 0, 't'},
    {"verbose",    no_argument,       0, 'v'},
    {"version",    no_argument,       0, 'V'},
    {0, 0, 0, 0}
  };
/* *INDENT-ON* */
static const char short_options[] = "hcd:fm:st:vV";

//=============================================================================

void
PlaneFromWinding (winding_t * w, plane_t * plane)
{
  vec3_t v1, v2;

// calc plane
  VectorSubtract (w->points[2], w->points[1], v1);
  VectorSubtract (w->points[0], w->points[1], v2);
  DotProduct (v1, v2);
  CrossProduct (v2, v1, plane->normal);
  VectorNormalize (plane->normal, plane->normal);
  plane->dist = DotProduct (w->points[0], plane->normal);
}


/*
==================
NewWinding
==================
*/
winding_t *
NewWinding (int points)
{
  winding_t *w;
  int size;

  if (points > MAX_POINTS_ON_WINDING)
    Error ("NewWinding: %i points", points);

  size = (intptr_t) ((winding_t *) 0)->points[points];
  w = malloc (size);
  memset (w, 0, size);

  return w;
}



void
pw (winding_t * w)
{
  int i;
  for (i = 0; i < w->numpoints; i++)
    printf ("(%5.1f, %5.1f, %5.1f)\n", w->points[i][0], w->points[i][1],
	    w->points[i][2]);
}

void
prl (leaf_t * l)
{
  int i;
  portal_t *p;
  plane_t pl;

  for (i = 0; i < l->numportals; i++)
    {
      p = l->portals[i];
      pl = p->plane;
      printf ("portal %4i to leaf %4i : %7.1f : (%4.1f, %4.1f, %4.1f)\n",
	      (int) (p - portals), p->leaf, pl.dist, pl.normal[0],
	      pl.normal[1], pl.normal[2]);
    }
}


//=============================================================================

/*
=============
SortPortals

Sorts the portals from the least complex, so the later ones can reuse
the earlier information.
=============
*/
int
PComp (const void *a, const void *b)
{
  if ((*(portal_t **) a)->nummightsee == (*(portal_t **) b)->nummightsee)
    return 0;
  if ((*(portal_t **) a)->nummightsee < (*(portal_t **) b)->nummightsee)
    return -1;
  return 1;
}

void
SortPortals (void)
{
  int i;

  for (i = 0; i < numportals * 2; i++)
    sorted_portals[i] = &portals[i];

  if (nosort)
    return;
  qsort (sorted_portals, numportals * 2, sizeof (sorted_portals[0]), PComp);
}


/*
==============
LeafVectorFromPortalVector
==============
*/
int
LeafVectorFromPortalVector (byte * portalbits, byte * leafbits)
{
  int i;
  portal_t *p;
  int c_leafs;


  memset (leafbits, 0, leafbytes);

  for (i = 0; i < numportals * 2; i++)
    {
      if (portalbits[i >> 3] & (1 << (i & 7)))
	{
	  p = portals + i;
	  leafbits[p->leaf >> 3] |= (1 << (p->leaf & 7));
	}
    }

  c_leafs = CountBits (leafbits, portalclusters);

  return c_leafs;
}


/*
===============
ClusterMerge

Merges the portal visibility for a leaf
===============
*/
void
ClusterMerge (int leafnum)
{
  leaf_t *leaf;
  byte portalvector[MAX_PORTALS / 8];
  byte uncompressed[MAX_MAP_LEAFS / 8];
  byte compressed[MAX_MAP_LEAFS / 8];
  int i, j;
  int numvis;
  byte *dest;
  portal_t *p;
  int pnum;

  // OR together all the portalvis bits

  memset (portalvector, 0, portalbytes);
  leaf = &leafs[leafnum];
  for (i = 0; i < leaf->numportals; i++)
    {
      p = leaf->portals[i];
      if (p->status != stat_done)
	Error ("portal not done");
      for (j = 0; j < portallongs; j++)
	((long *) portalvector)[j] |= ((long *) p->portalvis)[j];
      pnum = p - portals;
      portalvector[pnum >> 3] |= 1 << (pnum & 7);
    }

  // convert portal bits to leaf bits
  numvis = LeafVectorFromPortalVector (portalvector, uncompressed);

  if (uncompressed[leafnum >> 3] & (1 << (leafnum & 7)))
    printf ("WARNING: Leaf portals saw into leaf (%d)\n", leafnum);

  uncompressed[leafnum >> 3] |= (1 << (leafnum & 7));
  numvis++;			// count the leaf itself

  // save uncompressed for PHS calculation
  memcpy (uncompressedvis + leafnum * leafbytes, uncompressed, leafbytes);

//
// compress the bit string
//
  qprintf ("cluster %4i : %4i visible\n", leafnum, numvis);
  totalvis += numvis;

  i = CompressVis (uncompressed, compressed);

  dest = vismap_p;
  vismap_p += i;

  if (vismap_p > vismap_end)
    Error ("Vismap expansion overflow");

  dvis->bitofs[leafnum][DVIS_PVS] = dest - vismap;

  memcpy (dest, compressed, i);
}


/*
==================
CalcPortalVis
==================
*/
void
CalcPortalVis (void)
{
  int i;

// fastvis just uses mightsee for a very loose bound
  if (fastvis)
    {
      for (i = 0; i < numportals * 2; i++)
	{
	  portals[i].portalvis = portals[i].portalflood;
	  portals[i].status = stat_done;
	}
      return;
    }

  RunThreadsOnIndividual (numportals * 2, true, PortalFlow);

}


/*
==================
CalcVis
==================
*/
void
CalcVis (void)
{
  int i;

  RunThreadsOnIndividual (numportals * 2, true, BasePortalVis);

//      RunThreadsOnIndividual (numportals*2, true, BetterPortalVis);

  SortPortals ();

  CalcPortalVis ();

//
// assemble the leaf vis lists by oring and compressing the portal lists
//
  for (i = 0; i < portalclusters; i++)
    ClusterMerge (i);

  printf ("Average clusters visible: %i\n", totalvis / portalclusters);
}


void
SetPortalSphere (portal_t * p)
{
  int i;
  vec3_t total, dist;
  winding_t *w;
  float r, bestr;

  w = p->winding;
  VectorCopy (vec3_origin, total);
  for (i = 0; i < w->numpoints; i++)
    {
      VectorAdd (total, w->points[i], total);
    }

  for (i = 0; i < 3; i++)
    total[i] /= w->numpoints;

  bestr = 0;
  for (i = 0; i < w->numpoints; i++)
    {
      VectorSubtract (w->points[i], total, dist);
      r = VectorLength (dist);
      if (r > bestr)
	bestr = r;
    }
  VectorCopy (total, p->origin);
  p->radius = bestr;
}

/*
============
LoadPortals
============
*/
void
LoadPortals (char *name)
{
  int i, j;
  portal_t *p;
  leaf_t *l;
  char magic[80];
  FILE *f;
  int numpoints;
  winding_t *w;
  int leafnums[2];
  plane_t plane;

  if (!strcmp (name, "-"))
    f = stdin;
  else
    {
      f = fopen (name, "r");
      if (!f)
	Error ("LoadPortals: couldn't read %s\n", name);
    }

  if (fscanf (f, "%79s\n%i\n%i\n", magic, &portalclusters, &numportals) != 3)
    Error ("LoadPortals: failed to read header");
  if (strcmp (magic, PORTALFILE))
    Error ("LoadPortals: not a portal file");

  printf ("%4i portalclusters\n", portalclusters);
  printf ("%4i numportals\n", numportals);

  // these counts should take advantage of 64 bit systems automatically
  leafbytes = ((portalclusters + 63) & ~63) >> 3;
  leaflongs = leafbytes / sizeof (long);

  portalbytes = ((numportals * 2 + 63) & ~63) >> 3;
  portallongs = portalbytes / sizeof (long);

// each file portal is split into two memory portals
  portals = malloc (2 * numportals * sizeof (portal_t));
  memset (portals, 0, 2 * numportals * sizeof (portal_t));

  leafs = malloc (portalclusters * sizeof (leaf_t));
  memset (leafs, 0, portalclusters * sizeof (leaf_t));

  originalvismapsize = portalclusters * leafbytes;
  uncompressedvis = malloc (originalvismapsize);

  vismap = vismap_p = dvisdata;
  dvis->numclusters = portalclusters;
  vismap_p = (byte *) & dvis->bitofs[portalclusters];

  vismap_end = vismap + MAX_MAP_VISIBILITY;

  for (i = 0, p = portals; i < numportals; i++)
    {
      if (fscanf (f, "%i %i %i ", &numpoints, &leafnums[0], &leafnums[1])
	  != 3)
	Error ("LoadPortals: reading portal %i", i);
      if (numpoints > MAX_POINTS_ON_WINDING)
	Error ("LoadPortals: portal %i has too many points", i);
      if ((unsigned) leafnums[0] > portalclusters
	  || (unsigned) leafnums[1] > portalclusters)
	Error ("LoadPortals: reading portal %i", i);

      w = p->winding = NewWinding (numpoints);
      w->original = true;
      w->numpoints = numpoints;

      for (j = 0; j < numpoints; j++)
	{
	  double v[3];
	  int k;

	  // scanf into double, then assign to vec_t
	  // so we don't care what size vec_t is
	  if (fscanf (f, "(%lf %lf %lf ) ", &v[0], &v[1], &v[2]) != 3)
	    Error ("LoadPortals: reading portal %i", i);
	  for (k = 0; k < 3; k++)
	    w->points[j][k] = v[k];
	}
      fscanf (f, "\n");

      // calc plane
      PlaneFromWinding (w, &plane);

      // create forward portal
      l = &leafs[leafnums[0]];
      if (l->numportals == MAX_PORTALS_ON_LEAF)
	Error ("Leaf with too many portals");
      l->portals[l->numportals] = p;
      l->numportals++;

      p->winding = w;
      VectorSubtract (vec3_origin, plane.normal, p->plane.normal);
      p->plane.dist = -plane.dist;
      p->leaf = leafnums[1];
      p->owner_leaf = leafnums[0];
      SetPortalSphere (p);
      p++;

      // create backwards portal
      l = &leafs[leafnums[1]];
      if (l->numportals == MAX_PORTALS_ON_LEAF)
	Error ("Leaf with too many portals");
      l->portals[l->numportals] = p;
      l->numportals++;

      p->winding = NewWinding (w->numpoints);
      p->winding->numpoints = w->numpoints;
      for (j = 0; j < w->numpoints; j++)
	{
	  VectorCopy (w->points[w->numpoints - 1 - j], p->winding->points[j]);
	}

      p->plane = plane;
      p->leaf = leafnums[0];
      p->owner_leaf = leafnums[1];
      SetPortalSphere (p);
      p++;

    }

  fclose (f);
}


/*
================
CalcPHS

Calculate the PHS (Potentially Hearable Set)
by ORing together all the PVS visible from a leaf
================
*/
void
CalcPHS (void)
{
  int i, j, k, l, index;
  int bitbyte;
  long *dest, *src;
  byte *scan;
  int count;
  byte uncompressed[MAX_MAP_LEAFS / 8];
  byte compressed[MAX_MAP_LEAFS / 8];

  printf ("Building PHS...\n");

  count = 0;
  for (i = 0; i < portalclusters; i++)
    {
      scan = uncompressedvis + i * leafbytes;
      memcpy (uncompressed, scan, leafbytes);
      for (j = 0; j < leafbytes; j++)
	{
	  bitbyte = scan[j];
	  if (!bitbyte)
	    continue;
	  for (k = 0; k < 8; k++)
	    {
	      if (!(bitbyte & (1 << k)))
		continue;
	      // OR this pvs row into the phs
	      index = ((j << 3) + k);
	      if (index >= portalclusters)
		Error ("Bad bit in PVS");	// pad bits should be 0
	      src = (long *) (uncompressedvis + index * leafbytes);
	      dest = (long *) uncompressed;
	      for (l = 0; l < leaflongs; l++)
		((long *) uncompressed)[l] |= src[l];
	    }
	}
      for (j = 0; j < portalclusters; j++)
	if (uncompressed[j >> 3] & (1 << (j & 7)))
	  count++;

      //
      // compress the bit string
      //
      j = CompressVis (uncompressed, compressed);

      dest = (long *) vismap_p;
      vismap_p += j;

      if (vismap_p > vismap_end)
	Error ("Vismap expansion overflow");

      dvis->bitofs[i][DVIS_PHS] = (byte *) dest - vismap;

      memcpy (dest, compressed, j);
    }

  printf ("Average clusters hearable: %i\n", count / portalclusters);
}

/*
============
ShowUsage
============
*/

void
ShowUsage ()
{
  printf ("\nUsage: q2vis [options] bspfile\n\n"
	  "    -d, -maxdist <distance>\n"
	  "    -c, --cullerror\n"
	  "    -f, --fast\n"
	  "    -t, --threads #\n"
	  "    -s, --nosort\n"
	  "    -v, --verbose\n"
	  "    -m, --maxfovdist <fov> <distance>\n\n"
	  "Report bugs to <" PACKAGE_BUGREPORT ">.\n\n");
  exit (1);
}

/*
===========
main
===========
*/
int
main (int argc, char **argv)
{
  char portalfile[FN_SIZE];
  char bspfile[FN_SIZE];
  char *target;
  double start, end;

  verbose = false;

  while (1)
    {

      int c, option_index = 0;
      float fov, tmpmaxdist;

      c = getopt_long (argc, argv, short_options,
		       long_options, &option_index);

      if (c == -1)
	break;

      switch (c)
	{
	case 'c':
	  cullerror = true;
	  break;
	case 'd':
	  sscanf (optarg, "%f", &maxdist);
	  break;
	case 'f':
	  fastvis = true;
	  break;
	case 'h':
	  ShowUsage ();
	  break;
	case 'm':
	  sscanf (optarg, "%f,%f", &fov, &tmpmaxdist);
	  maxdist = tmpmaxdist /
	    (float) cos (atan (tan (fov * Q_PI / 360.0) * 5.0 / 4.0));
	  break;
	case 's':
	  nosort = true;
	  break;
	case 't':
	  sscanf (optarg, "%i", &numthreads);
	  break;
	case 'v':
	  verbose = true;
	  break;
	case 'V':
	  ShowVersion (Q2VIS_STRING);
	  break;
	case '?':
	  /* `getopt_long' already printed an error message. */
	default:
	  ShowUsage ();
	}
    }

  /* Get map filename(s) and process each in turn */
  if (optind >= argc)
    {
      printf ("You must specify an input BSP file.\n");
      ShowUsage ();
    }

  target = argv[optind];

  start = I_FloatTime ();

  ThreadSetDefault ();

//    if(!IsFullPath(target))
//          SetQdirFromPath (target);    

  StripExtension (target);

  strcpy(bspfile, target);
  DefaultExtension (bspfile, ".bsp");

  printf ("Reading BSP: %s\n", bspfile);
  LoadBSPFile (bspfile);
  if (numnodes == 0 || numfaces == 0)
    Error ("Empty map");

  strcpy(portalfile, target);
  DefaultExtension (portalfile, ".prt");

  printf ("Reading portals: %s\n", portalfile);
  LoadPortals (portalfile);

  CalcVis ();

  CalcPHS ();

  visdatasize = vismap_p - dvisdata;
  printf ("visdatasize: %i  compressed from %i\n", visdatasize,
	  originalvismapsize * 2);

  printf ("Writing BSP: %s\n", bspfile);
  WriteBSPFile (bspfile);

  end = I_FloatTime ();
  printf ("%5.1f seconds elapsed\n", end - start);

  return 0;
}
