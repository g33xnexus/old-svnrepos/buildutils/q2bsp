/* q2bspinfo.c
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cmdlib.h"
#include "mathlib.h"
#include "bspfile.h"

#define Q2BSPINFO_STRING "Q2BSPINFO"

/* *INDENT-OFF* */
static const struct option long_options[] =
  {
    {"help",       no_argument,       0, 'h'},
    {"version",    no_argument,       0, 'V'},
    {0, 0, 0, 0}
  };
/* *INDENT-ON* */
static const char short_options[] = "hV";



void
ShowUsage ()
{
  printf ("\nUsage: bspinfo bspfile [bspfiles...]\n\n"
	  "    -h, --help             -V, --version\n\n"
	  "Report bugs to <" PACKAGE_BUGREPORT ">.\n\n");
  exit (1);
}



int
main (int argc, char **argv)
{
  int i;
  char source[1024];
  int size;
  FILE *f;

  while (1)
    {

      int c, option_index = 0;

      c = getopt_long (argc, argv, short_options,
		       long_options, &option_index);

      if (c == -1)
	break;

      switch (c)
	{
	case 'h':
	  ShowUsage ();
	  break;
	case 'V':
	  ShowVersion (Q2BSPINFO_STRING);
	  break;
	case '?':
	  /* `getopt_long' already printed an error message. */
	default:
	  ShowUsage ();
	}
    }

  if (optind >= argc)
    {
      printf ("You must specify at least one BSP file to examine\n");
      ShowUsage ();
    }

  for (i = optind; i < argc; i++)
    {
      printf ("---------------------\n");
      strcpy (source, argv[i]);
      DefaultExtension (source, ".bsp");
      f = fopen (source, "rb");
      if (f)
	{
	  size = Q_filelength (f);
	  fclose (f);
	}
      else
	size = 0;
      printf ("%s: %i\n", source, size);

      LoadBSPFile (source);
      PrintBSPFileSizes ();
      printf ("---------------------\n");
    }

  return 0;
}
