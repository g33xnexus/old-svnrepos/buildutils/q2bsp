/* q2bsp.c
 *
 * buildutils-q2bsp: Compile levels for Quake II
 * Copyright (C) 1994-2001 iD Software
 * Copyright (C) 2005-2006 Peter Brett
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "q2bsp.h"
#include "parsecfg.h"

#define Q2BSP_STRING "Q2BSP"

extern float subdivide_size;

char basename[FN_SIZE];
char mapfile[FN_SIZE];

vec_t microvolume = 1.0;
qboolean noprune = false;
qboolean glview = false;
qboolean nodetail = false;
qboolean fulldetail = false;
qboolean onlyents = false;
qboolean nomerge = false;
qboolean nowater = false;
qboolean nofill = false;
qboolean nocsg = false;
qboolean noweld = false;
qboolean noshare = false;
qboolean nosubdiv = false;
qboolean notjunc = false;
qboolean noopt = false;
qboolean leaktest = false;
qboolean verboseentities = false;
qboolean badnormal_check = false;
qboolean origfix = false;

float badnormal;

char outbase[32] = "";

int block_xl = -8, block_xh = 7, block_yl = -8, block_yh = 7;

int entity_num;


node_t *block_nodes[10][10];

/* *INDENT-OFF* */
static const struct option long_options[] =
  {
    {"blocks",     required_argument, 0, 'b'},
    {"chop",       required_argument, 0, 'c'},
    {"nodetail",   no_argument,       0, 'd'},
    {"onlyents",   no_argument,       0, 'e'},
    {"fulldetail", no_argument,       0, 'f'},
    {"gamedir",    required_argument, 0, 'g'},
    {"help",       no_argument,       0, 'h'},
    {"notjunct",   no_argument,       0, 'j'},
    {"leaktest",   no_argument,       0, 'l'},
    {"moddir",     required_argument, 0, 'm'},
    {"badnormal",  required_argument, 0, 'n'},
    {"origfix",    no_argument,       0, 'o'},
    {"noprune",    no_argument,       0, 'p'},
    {"noshare",    no_argument,       0, 's'},
    {"threads",    required_argument, 0, 't'},
    {"micro",      required_argument, 0, 'u'},
    {"verbose",    no_argument,       0, 'v'},
    {"nowater",    no_argument,       0, 'w'},
    {"block",      required_argument, 0, 'B'},
    {"nocsg",      no_argument,       0, 'C'},
    {"draw",       no_argument,       0, 'D'},
    {"verboseents", no_argument,      0, 'E'},
    {"nofill",     no_argument,       0, 'F'},
    {"glview",     no_argument,       0, 'G'},
    {"nomerge",    no_argument,       0, 'M'},
    {"noopt",      no_argument,       0, 'O'},
    {"nosubdiv",   no_argument,       0, 'S'},
    {"version",    no_argument,       0, 'V'},
    {"noweld",     no_argument,       0, 'W'},
    {0, 0, 0, 0}
  };
/* *INDENT-ON* */
static const char short_options[] =
  "b:c:defg:h:jlm:n:opst:u:vwB:CDEFGMOST:VW";

/*
============
BlockTree

============
*/
node_t *
BlockTree (int xl, int yl, int xh, int yh)
{
  node_t *node;
  vec3_t normal;
  float dist;
  int mid;

  if (xl == xh && yl == yh)
    {
      node = block_nodes[xl + 5][yl + 5];
      if (!node)
	{			// return an empty leaf
	  node = AllocNode ();
	  node->planenum = PLANENUM_LEAF;
	  node->contents = 0;	//CONTENTS_SOLID;
	  return node;
	}
      return node;
    }

  // create a seperator along the largest axis
  node = AllocNode ();

  if (xh - xl > yh - yl)
    {				// split x axis
      mid = xl + (xh - xl) / 2 + 1;
      normal[0] = 1;
      normal[1] = 0;
      normal[2] = 0;
      dist = mid * 1024;
      node->planenum = FindFloatPlane (normal, dist);
      node->children[0] = BlockTree (mid, yl, xh, yh);
      node->children[1] = BlockTree (xl, yl, mid - 1, yh);
    }
  else
    {
      mid = yl + (yh - yl) / 2 + 1;
      normal[0] = 0;
      normal[1] = 1;
      normal[2] = 0;
      dist = mid * 1024;
      node->planenum = FindFloatPlane (normal, dist);
      node->children[0] = BlockTree (xl, mid, xh, yh);
      node->children[1] = BlockTree (xl, yl, xh, mid - 1);
    }

  return node;
}

/*
============
ProcessBlock_Thread

============
*/
int brush_start, brush_end;
void
ProcessBlock_Thread (int blocknum)
{
  int xblock, yblock;
  vec3_t mins, maxs;
  bspbrush_t *brushes;
  tree_t *tree;
  node_t *node;

  yblock = block_yl + blocknum / (block_xh - block_xl + 1);
  xblock = block_xl + blocknum % (block_xh - block_xl + 1);

  qprintf ("############### block %2i,%2i ###############\n", xblock, yblock);

  mins[0] = xblock * 1024;
  mins[1] = yblock * 1024;
  mins[2] = -4096;
  maxs[0] = (xblock + 1) * 1024;
  maxs[1] = (yblock + 1) * 1024;
  maxs[2] = 4096;

  // the makelist and chopbrushes could be cached between the passes...
  brushes = MakeBspBrushList (brush_start, brush_end, mins, maxs);
  if (!brushes)
    {
      node = AllocNode ();
      node->planenum = PLANENUM_LEAF;
      node->contents = CONTENTS_SOLID;
      block_nodes[xblock + 5][yblock + 5] = node;
      return;
    }

//      if (!nocsg)
//              brushes = ChopBrushes (brushes);

  tree = BrushBSP (brushes, mins, maxs);

  block_nodes[xblock + 5][yblock + 5] = tree->headnode;

  free (tree);
}

/*
============
ProcessWorldModel

============
*/

void
ProcessWorldModel (void)
{
  entity_t *e;
  tree_t *tree;
  qboolean leaked;
  qboolean optimize;

  e = &entities[entity_num];

  brush_start = e->firstbrush;
  brush_end = brush_start + e->numbrushes;
  leaked = false;

  //
  // perform per-block operations
  //
  if (block_xh * 1024 > map_maxs[0])
    block_xh = floor (map_maxs[0] / 1024.0);
  if ((block_xl + 1) * 1024 < map_mins[0])
    block_xl = floor (map_mins[0] / 1024.0);
  if (block_yh * 1024 > map_maxs[1])
    block_yh = floor (map_maxs[1] / 1024.0);
  if ((block_yl + 1) * 1024 < map_mins[1])
    block_yl = floor (map_mins[1] / 1024.0);

  if (block_xl < -4)
    block_xl = -4;
  if (block_yl < -4)
    block_yl = -4;
  if (block_xh > 3)
    block_xh = 3;
  if (block_yh > 3)
    block_yh = 3;

  for (optimize = false; optimize <= true; optimize++)
    {
      qprintf ("--------------------------------------------\n");

      RunThreadsOnIndividual ((block_xh - block_xl + 1) * (block_yh -
							   block_yl + 1),
			      !verbose, ProcessBlock_Thread);

      //
      // build the division tree
      // oversizing the blocks guarantees that all the boundaries
      // will also get nodes.
      //

      qprintf ("--------------------------------------------\n");

      tree = AllocTree ();
      tree->headnode =
	BlockTree (block_xl - 1, block_yl - 1, block_xh + 1, block_yh + 1);

      tree->mins[0] = (block_xl) * 1024;
      tree->mins[1] = (block_yl) * 1024;
      tree->mins[2] = map_mins[2] - 8;

      tree->maxs[0] = (block_xh + 1) * 1024;
      tree->maxs[1] = (block_yh + 1) * 1024;
      tree->maxs[2] = map_maxs[2] + 8;

      //
      // perform the global operations
      //
      MakeTreePortals (tree);

      if (FloodEntities (tree))
	FillOutside (tree->headnode);
      else
	{
	  printf ("**** leaked ****\n");
	  leaked = true;
	  LeakFile (tree);
	  if (leaktest)
	    {
	      printf ("--- MAP LEAKED ---\n");
	      exit (0);
	    }
	}

      MarkVisibleSides (tree, brush_start, brush_end);
      if (noopt || leaked)
	break;
      if (!optimize)
	{
	  FreeTree (tree);
	}
    }

  FloodAreas (tree);
  if (glview)
    WriteGLView (tree, basename);
  MakeFaces (tree->headnode);
  FixTjuncs (tree->headnode);

  if (!noprune)
    PruneNodes (tree->headnode);

  WriteBSP (tree->headnode);

  if (!leaked)
    WritePortalFile (tree);

  FreeTree (tree);
}

/*
============
ProcessSubModel

============
*/
void
ProcessSubModel (void)
{
  entity_t *e;
  int start, end;
  tree_t *tree;
  bspbrush_t *list;
  vec3_t mins, maxs;

  e = &entities[entity_num];

  start = e->firstbrush;
  end = start + e->numbrushes;

  mins[0] = mins[1] = mins[2] = -4096;
  maxs[0] = maxs[1] = maxs[2] = 4096;
  list = MakeBspBrushList (start, end, mins, maxs);
  if (!nocsg)
    list = ChopBrushes (list);
  tree = BrushBSP (list, mins, maxs);
  MakeTreePortals (tree);
  MarkVisibleSides (tree, start, end);
  MakeFaces (tree->headnode);
  FixTjuncs (tree->headnode);
  WriteBSP (tree->headnode);
  FreeTree (tree);
}

/*
============
ProcessModels
============
*/
void
ProcessModels (void)
{
  BeginBSPFile ();

  for (entity_num = 0; entity_num < num_entities; entity_num++)
    {
      if (!entities[entity_num].numbrushes)
	continue;

      qprintf ("############### model %i ###############\n", nummodels);
      BeginModel ();
      if (entity_num == 0)
	ProcessWorldModel ();
      else
	ProcessSubModel ();
      EndModel ();

      if (!verboseentities)
	verbose = false;	// don't bother printing submodels
    }

  EndBSPFile ();
}

/*
============
ShowUsage
============
*/
void
ShowUsage ()
{

  printf ("\nUsage: q2bsp [options] mapfile\n\n"
	  "    -n, --badnormal #      -u, --micro #        -p, --noprune\n"
	  "    -b, --block #,#        -m, --moddir <path>  -j, --notjunc\n"
	  "    -B, --blocks #,#,#,#   -C, --nocsg          -w, --nowater\n"
	  "    -c, --chop #           -d, --nodetail       -W, --noweld\n"
	  "    -D, --draw (disabled)  -F, --nofill         -e, --onlyents\n"
	  "    -g, --gamedir <path>   -M, --nomerge        -t, --threads (disabled)\n"
	  "    -h, --help             -s, --noshare        -l, --leaktest\n"
	  "    -f, --fulldetail       -S, --nosubdiv       -v, --verbose\n"
	  "    -G, --glview           -O, --noopt          -E, --verboseents\n"
	  "    -o, --origfix          -V, --version\n\n"
	  "Report bugs to <" PACKAGE_BUGREPORT ">.\n\n");
  exit (1);
}

/*
============
main
============
*/
int
main (int argc, char **argv)
{
  int n;
  double start, end;
  char path[FN_SIZE];
  char gamedir[FN_SIZE] = "";
  char *target;
  char moddir[FN_SIZE];

  numthreads = 1;

  while (1)
    {

      int c, option_index = 0;

      c = getopt_long (argc, argv, short_options,
		       long_options, &option_index);

      if (c == -1)
	break;

      switch (c)
	{
	case 'b':
	  sscanf (optarg, "%i,%i", &block_xl, &block_yl);
	  break;
	case 'c':
	  sscanf (optarg, "%f", &subdivide_size);
	  break;
	case 'd':
	  nodetail = true;
	  break;
	case 'e':
	  onlyents = true;
	  break;
	case 'f':
	  fulldetail = true;
	  break;
	case 'g':
	  strncpy (gamedir, optarg, 1024);
	  break;
	case 'h':
	  ShowUsage ();
	  break;
	case 'j':
	  notjunc = true;
	  break;
	case 'l':
	  leaktest = true;
	  break;
	case 'm':
	  strncpy (moddir, optarg, 1024);
	  break;
	case 'n':
	  sscanf (optarg, "%f", &badnormal);
	  badnormal_check = 1;
	  break;
	case 'o':
	  origfix = true;
	  break;
	case 'p':
	  noprune = true;
	  break;
	case 's':
	  noshare = true;
	  break;
	case 't':
	  /* // Do nothing, no performance gain from multithreading
	   * sscanf (optarg, "%i", &numthreads);
	   */
	  break;
	case 'u':
	  sscanf (optarg, "%f", &microvolume);
	  break;
	case 'v':
	  verbose = true;
	  break;
	case 'w':
	  nowater = true;
	  break;
	case 'B':
	  sscanf (optarg, "%i,%i-%i,%i", &block_xl, &block_yl,
		  &block_xh, &block_yh);
	  break;
	case 'C':
	  nocsg = true;
	  break;
	case 'D':
	  /* // Do nothing, draw is disabled
	   * drawflag = true;
	   */
	  break;
	case 'E':
	  verboseentities = true;
	  break;
	case 'F':
	  nofill = true;
	  break;
	case 'G':
	  glview = true;
	  break;
	case 'M':
	  nomerge = true;
	  break;
	case 'O':
	  noopt = true;
	  break;
	case 'S':
	  nosubdiv = true;
	  break;
	case 'V':
	  ShowVersion (Q2BSP_STRING);
	  break;
	case 'W':
	  noweld = true;
	  break;
	case '?':
	  /* `getopt_long' already printed an error message. */
	default:
	  ShowUsage ();
	}
    }

  /* Check user actually specified a map to process */
  if (optind >= argc)
    {
      printf ("You must specify an input MAP file.\n");
      ShowUsage ();
    }

  start = I_FloatTime ();

  ThreadSetDefault ();
  numthreads = 1;		// multiple threads aren't helping...
    
  /* Set up locations of game data files */
  printf ("gamedir = %s\n", gamedir);
  
  if (moddir[0] != 0)
    {
      printf ("moddir = %s\n", moddir);
    }
  InitGameData(gamedir, moddir);
      

  /* Process each input file in turn */
  for ( ; optind < argc ; optind++ )
    {
      strcpy(basename, argv[optind]);

      StripExtension (basename);

      // delete portal and line files
      sprintf (path, "%s.prt", basename);
      remove (path);
      sprintf (path, "%s.lin", basename);
      remove (path);

      strcpy (mapfile, basename);
      DefaultExtension (mapfile, ".map");	// might be .reg

      //
      // if onlyents, just grab the entites and resave
      //
      if (onlyents)
	{
	  sprintf (path, "%s.bsp", basename);
	  LoadBSPFile (path);
	  num_entities = 0;

	  LoadMapFile (mapfile);
	  SetModelNumbers ();
	  SetLightStyles ();

	  UnparseEntities ();

	  WriteBSPFile (path);
	}
      else
	{
	  //
	  // start from scratch
	  //
	  LoadMapFile (mapfile);

	  SetModelNumbers ();
	  SetLightStyles ();

	  ProcessModels ();
	}
    }

  end = I_FloatTime ();
  printf ("%5.0f seconds elapsed\n", end - start);

  return 0;
}
